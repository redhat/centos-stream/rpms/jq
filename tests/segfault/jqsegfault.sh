#!/bin/sh

FAIL=0

# Test for segfault in multithreaded environment
gcc -o test_segfault test_segfault_with_multithreaded_env.c -lpthread -ljq && \
./test_segfault
RET=$?

echo -n "Test jqsegfault ... "

if [ $RET != 0 ]; then
    echo "failed"
    FAIL=1
else
    echo "ok"
fi

exit $FAIL
