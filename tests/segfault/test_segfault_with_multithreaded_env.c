#include <stdlib.h>
#include <jq.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

int my_jq_parse(jq_state *jq, struct jv_parser *parser)
{
    int rv = 0;
    jv value;

    value = jv_parser_next(parser);
    while (jv_is_valid(value)) {
        jq_start(jq, value, 0);
        jv result;

        while (jv_is_valid(result = jq_next(jq))) {
            jv dumped = jv_dump_string(result, 0);
            const char *str = jv_string_value(dumped);
            printf("dumped: %s\n", str);
        }

        jv_free(result);
        value = jv_parser_next(parser);
    }

    if (jv_invalid_has_msg(jv_copy(value))) {
        jv msg = jv_invalid_get_msg(value);
        printf("invalid: %s\n", jv_string_value(msg));
        jv_free(msg);
        rv = 1;
    } else {
        jv_free(value);
    }

    return rv;
}

void *run(void *not_used) {
    int rv;
    jq_state *jq;
    const char *prg = ".data";

    jq = jq_init();
    printf("jq_init jq: %p prg: %s\n", jq, prg);
    if (jq_compile(jq, prg) == 0) {
        jq_teardown(&jq);
        return NULL;
    }
    printf("compiled\n");

    struct jv_parser *parser = jv_parser_new(0);
    const char *buf = "{ \"data\": 1 }";
    jv_parser_set_buf(parser, buf, strlen(buf), 0);
    rv = my_jq_parse(jq, parser);
    if (rv != 0) {
        printf("my_jq_parse failed!\n");
    }

    jv_parser_free(parser);
    jq_teardown(&jq);
    return NULL;
}

#define THREADS 2
/* calling run() twice works fine */
/* calling them in threads causes core-dump */
int main (int argc, char *argv[])
{
    pthread_t threads[THREADS];
    int createerror;
    int a;

     memset(&threads, 0, sizeof(threads));
    for (a = 0; a < THREADS; ++a) {
        // sleep(1); // < if you want to run threads sequentionally 
        createerror = pthread_create(&threads[a], NULL, run, NULL);
        if (createerror) {
            printf("create thread error %d\n", a);
        }
    }
    for(a = 0; a < THREADS; ++a) {
        if (threads[a] != 0) {
            pthread_join(threads[a], NULL);
        }
    }

    return 0;
}
