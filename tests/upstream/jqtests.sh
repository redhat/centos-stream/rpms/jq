#!/bin/sh

die () {
    echo "$1" >&2
    exit 1
}

dnf list jq
dnf remove -y jq
dnf --disablerepo=fedora* --disablerepo=updates* install -y jq jq-devel

# If source not found, download it with dnf
if [ ! -d ./source ]; then
    # Extract source from srpm
    dnf --disablerepo=fedora* --disablerepo=updates* download --source jq && \
    rpm2cpio jq*.rpm|cpio -id && \
    mkdir source && \
    tar -zxf jq-*.tar.gz -C source --strip-components=1
    if [ $? != 0 ]; then
        echo "Failed to download upstream tests"
        exit 1
    fi
fi

pushd ./source || die "missing source directory"
rm -f jq tests/*.log 2>/dev/null
ln -s /usr/bin/jq || die "failed to link jq binary"

FAIL=0

# run the tests
# List of tests is taken from Makefile
TESTS="tests/optionaltest tests/mantest tests/jqtest tests/onigtest tests/shtest tests/utf8test tests/base64test"

for t in $TESTS; do
    echo -n "Test $t ... "
    ./${t} >"${t}.log" 2>&1
    RET=$?
    if [ $RET = 0 ]; then
        echo "ok"
    else
        echo "failed"
        echo "-------------------- ${t}.log start -----------------------------"
        cat "${t}.log"
        echo "-------------------- ${t}.log end -----------------------------"
        FAIL=1
    fi
done

popd # exit SOURCE_DIR

exit $FAIL
