#!/bin/bash

export LC_ALL=C

stress_jq() {
    `which time` -p -f '%e\n' bash -c 'for i in `seq 1 1000` ; do echo '"'"'{"foo":"bar"}'"'"' | jq '"'"'has("bar")'"'"' > /dev/null ; done' 2>&1 | cut -d. -f1
}

FAIL=0
TIME=`stress_jq`
echo -n "Test jqspeed ... "
if [ $TIME -gt 8 ] ; then
    echo "failed"
    FAIL=1
else
    echo "ok"
fi

echo "Runtime: ${TIME}s "
exit $FAIL
